angular.module('sms', ['homeCtrl',
                        'studentListCtrl',
                        'singleStudentCtrl',
                        'frontRoutes',
                        'ngAnimate',
                        'studentService',
                        'addStudentCtrl']);