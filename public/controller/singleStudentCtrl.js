angular.module('singleStudentCtrl', ['ui.router'])
    .controller('singleStudentCtrl', singleStudentDetail);

function singleStudentDetail(studentService, $stateParams) {
    var vm = this;
    var studentId = $stateParams.studentId;

    // var id = '57176011117b429c0d4820a2';
    studentService.get(studentId).success(function (data) {
        vm.oneStudent = data;
    });

};
