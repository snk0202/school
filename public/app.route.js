angular.module('frontRoutes', ['ui.router'])
    .config(["$stateProvider", "$urlRouterProvider",
            function ($stateProvider, $urlRouteProvider, studentService) {
                $urlRouteProvider.otherwise('/');

                $stateProvider

                    .state('home',{
                        url:'/',
                        templateUrl: 'views/home.html',
                        controller:'homeCtrl as vm'
                    })

                    .state('singleStudent', {
                        url: '/singleStudent/:studentId',
                        templateUrl: 'views/singleStudent.html',
                        controller: 'singleStudentCtrl as vm'
                    })

                    .state('studentList', {
                        url: '/studentList',
                        templateUrl: 'views/studentList.html',
                        controller: 'studentListCtrl as vm'
                    })
                    .state('addStudent', {
                        url:'/addStudent',
                        templateUrl: 'views/addStudent.html',
                        controller: 'addStudentCtrl as vm'
                    });

            }]);
