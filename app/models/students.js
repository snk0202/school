var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
    Name: String,
    Fname:String,
    Mname:String,
    Class:Number,
    Address:String,
    Contact:Number,
    AdmissionYear:Number,
    LastYearResult:Number
});

module.exports = mongoose.model('Student', studentSchema);