// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var config = require('./config');
var Student = require('./app/models/students');

mongoose.connect(config.db ,function (err) {
    if(err){
        console.log(err);
    }else{
        console.log('connected to Mongolab!!');
    }
});

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

router.use(function (req, res, next) {
    console.log('Someone is doing CRUD');
    next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.route('/students')
    .post(function(req, res){
        var student = new Student();

        student.Name = req.body.Name;
        student.Fname = req.body.Fname;
        student.Mname = req.body.Mname;
        student.Class = req.body.Class;
        student.Address = req.body.Address;
        student.Contact = req.body.Contact;
        student.AdmissionYear = req.body.AdmissionYear;
        student.LastYearResult = req.body.LastYearResult;
        
        student.save(function (err) {
            if(err){
                res.send(err);
                return;
            }
            res.json({message:'Student created'});
        });
    })

    .get(function (req, res) {
        Student.find(function (err, studs) {
            if(err)
                res.send(err);
            res.json(studs);
        });
    });

    router.route('/students/:student_id')

        .get(function (req, res) {
        Student.findById(req.params.student_id, function (err, stud) {
            if(err){
                res.send(err);
                return;
            }

            res.send(stud);
        });
    })

        .put(function (req, res) {
            Student.findById(req.params.student_id, function(err, stud){
                if(err)
                    res.send(err);
                stud.Name = req.body.Name;
                stud.Fname = req.body.Fname;
                stud.Mname = req.body.Mname;
                stud.Class = req.body.Class;
                stud.Address = req.body.Address;
                stud.Contact = req.body.Contact;
                stud.AdmissionYear = req.body.AdmissionYear;
                stud.LastYearResult = req.body.LastYearResult;


                stud.save(function (err) {
                    if(err)
                        res.send(err);
                    res.json({message:'Student Updated'});

                });
            });
        })

        .delete(function(req, res){
            Student.remove({
                _id: req.params.student_id
            },function (err, student) {
                if(err)
                    res.send(err);
                res.json({message:'Student Sucessfully Deleted'});
            });
        });

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(config.PORT);
console.log('Magic happens on port ' + config.PORT);